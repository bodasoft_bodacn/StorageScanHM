package com.bodacn.storagescan;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;


import com.bodacn.storagescan.activity.CJLBActivity;
import com.bodacn.storagescan.activity.CRActivity;
import com.bodacn.storagescan.activity.ManualOperationActivity;
import com.bodacn.storagescan.activity.NHActivity;
import com.bodacn.storagescan.activity.TailorCallActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    String serviceUrl="http://192.168.199.186/BDCOMM";
    String serviceUrlExt  = "/BDService.asmx?wsdl";
    String curLoginUserNo,curLoginUser,curLoginTime;//操作员所属部门等信息;
    JSONArray curUserPower,funcJsonArray;
    private MainApplication mApp = null;
    GridView grid_func;
    //自身界面需要;
    ImageButton btn_LoginClose;
    TextView txt_titleInv,frameHintText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        InitView();
        //获取登陆参数
        Bundle paramBundle = this.getIntent().getExtras();
        curLoginUserNo=paramBundle.getString("LoginUserNo");
        curLoginUser=paramBundle.getString("LoginUser");
        curLoginTime=paramBundle.getString("LoginTime");
        serviceUrl=paramBundle.getString("serviceUrl");
        serviceUrlExt=paramBundle.getString("serviceUrlExt");
        try {
            curUserPower=new JSONArray(paramBundle.getString("UserPower"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //根据权限加载功能列表
        try {
            funcJsonArray=new JSONArray("[]");
            if (curUserPower!=null){
                if (GetUserFuncPower("FuncInStorageA")==1) {
                    JSONObject tmpObject=new JSONObject();
                    tmpObject.put("FuncName","FuncInStorageA");
                    tmpObject.put("FuncImgName","in_storagea");
                    tmpObject.put("FuncCaption","单包入仓");
                    funcJsonArray.put(tmpObject);
                }
                if (GetUserFuncPower("FuncOutStorageA")==1) {
                    JSONObject tmpObject=new JSONObject();
                    tmpObject.put("FuncName","FuncOutStorageA");
                    tmpObject.put("FuncImgName","out_storagea");
                    tmpObject.put("FuncCaption","单包出仓");
                    funcJsonArray.put(tmpObject);
                }
                if (GetUserFuncPower("FuncTransDepart")==1) {
                    JSONObject tmpObject=new JSONObject();
                    tmpObject.put("FuncName","FuncTransDepart");
                    tmpObject.put("FuncImgName","transdepart");
                    tmpObject.put("FuncCaption","转组扫描");
                    funcJsonArray.put(tmpObject);
                }
                if (GetUserFuncPower("FuncInStorageB")==1) {
                    JSONObject tmpObject=new JSONObject();
                    tmpObject.put("FuncName","FuncInStorageB");
                    tmpObject.put("FuncImgName","in_storageb");
                    tmpObject.put("FuncCaption","整单入仓");
                    funcJsonArray.put(tmpObject);
                }
                if (GetUserFuncPower("FuncOutStorageB")==1) {
                    JSONObject tmpObject=new JSONObject();
                    tmpObject.put("FuncName","FuncOutStorageB");
                    tmpObject.put("FuncImgName","out_storageb");
                    tmpObject.put("FuncCaption","整单出仓");
                    funcJsonArray.put(tmpObject);
                }
                if (GetUserFuncPower("FuncPartOut")==1) {
                    JSONObject tmpObject=new JSONObject();
                    tmpObject.put("FuncName","FuncPartOut");
                    tmpObject.put("FuncImgName","partout");
                    tmpObject.put("FuncCaption","部件出仓");
                    funcJsonArray.put(tmpObject);
                }
                if (GetUserFuncPower("Cut2Templet")==1) {
                    JSONObject tmpObject=new JSONObject();
                    tmpObject.put("FuncName","Cut2Templet");
                    tmpObject.put("FuncImgName","partout");
                    tmpObject.put("FuncCaption","裁床发模板");
                    funcJsonArray.put(tmpObject);
                }
                if (GetUserFuncPower("Templet2Factory")==1) {
                    JSONObject tmpObject=new JSONObject();
                    tmpObject.put("FuncName","Templet2Factory");
                    tmpObject.put("FuncImgName","partout");
                    tmpObject.put("FuncCaption","模板发车间");
                    funcJsonArray.put(tmpObject);
                }
                if (GetUserFuncPower("FuncCartOut")==1) {
                    JSONObject tmpObject=new JSONObject();
                    tmpObject.put("FuncName","FuncCartOut");
                    tmpObject.put("FuncImgName","out_cart");
                    tmpObject.put("FuncCaption","推车出仓");
                    funcJsonArray.put(tmpObject);
                }
                if (GetUserFuncPower("FuncInBagSingle")==1) {
                    JSONObject tmpObject=new JSONObject();
                    tmpObject.put("FuncName","InBagSingle");
                    tmpObject.put("FuncImgName","inbag");
                    tmpObject.put("FuncCaption","单包装袋");
                    funcJsonArray.put(tmpObject);
                }
                if (GetUserFuncPower("FuncInBagWhole")==1) {
                    JSONObject tmpObject=new JSONObject();
                    tmpObject.put("FuncName","InBagSingle");
                    tmpObject.put("FuncImgName","inbag");
                    tmpObject.put("FuncCaption","整单装袋");
                    funcJsonArray.put(tmpObject);
                }
                if (GetUserFuncPower("FuncCheck9999")==1) {
                    JSONObject tmpObject=new JSONObject();
                    tmpObject.put("FuncName","FuncCheck9999");
                    tmpObject.put("FuncImgName","check");
                    tmpObject.put("FuncCaption","裁床验片");
                    funcJsonArray.put(tmpObject);
                }
                if (GetUserFuncPower("FuncCheck9998")==1) {
                    JSONObject tmpObject=new JSONObject();
                    tmpObject.put("FuncName","FuncCheck9998");
                    tmpObject.put("FuncImgName","check");
                    tmpObject.put("FuncCaption","印花验片");
                    funcJsonArray.put(tmpObject);
                }
                if (GetUserFuncPower("FuncCheck9997")==1) {
                    JSONObject tmpObject=new JSONObject();
                    tmpObject.put("FuncName","FuncCheck9997");
                    tmpObject.put("FuncImgName","check");
                    tmpObject.put("FuncCaption","绣花验片");
                    funcJsonArray.put(tmpObject);
                }
                if (GetUserFuncPower("FuncCheck9996")==1) {
                    JSONObject tmpObject=new JSONObject();
                    tmpObject.put("FuncName","FuncCheck9996");
                    tmpObject.put("FuncImgName","check");
                    tmpObject.put("FuncCaption","特殊验片");
                    funcJsonArray.put(tmpObject);
                }
                if (GetUserFuncPower("FuncCheck8888")==1) {
                    JSONObject tmpObject=new JSONObject();
                    tmpObject.put("FuncName","FuncCheck8888");
                    tmpObject.put("FuncImgName","recsend");
                    tmpObject.put("FuncCaption","后道收发");
                    funcJsonArray.put(tmpObject);
                }

                if (GetUserFuncPower("FuncQueryStorage")==1) {
                    JSONObject tmpObject=new JSONObject();
                    tmpObject.put("FuncName","FuncQueryStorage");
                    tmpObject.put("FuncImgName","query_storage");
                    tmpObject.put("FuncCaption","仓库查询");
                    funcJsonArray.put(tmpObject);
                }
                if (GetUserFuncPower("FuncQueryTemplet")==1) {
                    JSONObject tmpObject=new JSONObject();
                    tmpObject.put("FuncName","FuncQueryTemplet");
                    tmpObject.put("FuncImgName","query_storage");
                    tmpObject.put("FuncCaption","模板接收查询");
                    funcJsonArray.put(tmpObject);
                }
                if (GetUserFuncPower("FuncSplitPack")==1) {
                    JSONObject tmpObject=new JSONObject();
                    tmpObject.put("FuncName","FuncSplitPack");
                    tmpObject.put("FuncImgName","splitpack");
                    tmpObject.put("FuncCaption","裁片拆包");
                    funcJsonArray.put(tmpObject);
                }
                //agv呼叫
                if (GetUserFuncPower("FuncManualOperation")==1) {
                    JSONObject tmpObject=new JSONObject();
                    tmpObject.put("FuncName","FuncManualOperation");
                    tmpObject.put("FuncImgName","out_cart");
                    tmpObject.put("FuncCaption","人工操作");
                    funcJsonArray.put(tmpObject);
                }
                //裁剪呼叫呼叫
                if (GetUserFuncPower("FuncTailorCall")==1) {
                    JSONObject tmpObject=new JSONObject();
                    tmpObject.put("FuncName","FuncTailorCall");
                    tmpObject.put("FuncImgName","recsend");
                    tmpObject.put("FuncCaption","裁剪呼叫");
                    funcJsonArray.put(tmpObject);
                }
                //粘合呼叫
                if (GetUserFuncPower("FuncNH")==1) {
                    JSONObject tmpObject=new JSONObject();
                    tmpObject.put("FuncName","FuncNH");
                    tmpObject.put("FuncImgName","splitpack");
                    tmpObject.put("FuncCaption","粘合呼叫");
                    funcJsonArray.put(tmpObject);
                }
                //充绒呼叫
                if (GetUserFuncPower("FuncCR")==1) {
                    JSONObject tmpObject=new JSONObject();
                    tmpObject.put("FuncName","FuncCR");
                    tmpObject.put("FuncImgName","recsend");
                    tmpObject.put("FuncCaption","充绒呼叫");
                    funcJsonArray.put(tmpObject);
                }
                //裁剪拉布
                if (GetUserFuncPower("FuncCJLB")==1) {
                    JSONObject tmpObject=new JSONObject();
                    tmpObject.put("FuncName","FuncCJLB");
                    tmpObject.put("FuncImgName","recsend");
                    tmpObject.put("FuncCaption","裁剪拉布");
                    funcJsonArray.put(tmpObject);
                }
                //裁剪拉布
                if (GetUserFuncPower("FuncYZSM")==1) {
                    JSONObject tmpObject=new JSONObject();
                    tmpObject.put("FuncName","FuncYZSM");
                    tmpObject.put("FuncImgName","splitpack");
                    tmpObject.put("FuncCaption","验针扫描");
                    funcJsonArray.put(tmpObject);
                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        //显示功能;
        if (funcJsonArray.length()>0){
            frameHintText.setVisibility(View.GONE);
            DetailGridAdapter detailGridAdapter=new DetailGridAdapter(this);
            detailGridAdapter.jsonArray=funcJsonArray;
            grid_func.setAdapter(detailGridAdapter);
        }

    }


    //订单表格适配器;
    public final class DetailGridAdapter extends BaseAdapter {
        private LayoutInflater inflater;
        public DetailGridAdapter(Context context) {
            inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        private JSONArray jsonArray;

        @Override
        public int getCount() {
            return jsonArray.length();
        }

        @Override
        public Object getItem(int position) {
            try {
                return jsonArray.get(position);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) convertView = inflater.inflate(R.layout.func_item, null, false);
            String tmpValue;
            try {
                TextView tv = convertView.findViewById(R.id.txt_func);// 显示文字
                if (jsonArray.optJSONObject(position).has("FuncCaption")==true)
                    tv.setText(jsonArray.optJSONObject(position).get("FuncCaption").toString());
                ImageView imv=convertView.findViewById(R.id.img_func);//图标
                if (jsonArray.optJSONObject(position).has("FuncImgName")==true)
                    if (getMipmapResource(jsonArray.optJSONObject(position).get("FuncImgName").toString())>0)
                    imv.setImageResource(getMipmapResource(jsonArray.optJSONObject(position).get("FuncImgName").toString()));
            } catch (JSONException e) {
                e.printStackTrace();
                System.out.println(e.getMessage());
            }
            Log.d("msg","DetailGridAdapter_position:"+position);
            return convertView;
        }

    }

    //获取资源ID
    public int  getMipmapResource(String imageName){
        Context ctx=getBaseContext();
        int resId = getResources().getIdentifier(imageName, "mipmap", ctx.getPackageName());
        //如果没有在"mipmap"下找到imageName,将会返回0
        return resId;
    }

    //获取权限,默认都有;
    private int GetUserFuncPower(String paramFunc){
        if (curUserPower.length()>0)
        {
            for (int i=0;i<curUserPower.length();i++){
                if (curUserPower.optJSONObject(i).has(paramFunc)==true)
                {
                    try {
                        return curUserPower.optJSONObject(i).getInt(paramFunc);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            return 0;
        }
        return 0;
    }

    private void InitView(){
        //全局App;
        mApp = (MainApplication) getApplication();
        //界面变量
        grid_func=findViewById(R.id.grid_func);
        grid_func.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    if (funcJsonArray.optJSONObject(i).getString("FuncName").equals("FuncInStorageA")==true){
                        Func_SingleScan("SingleIn","单包入仓");
                    }
                    if (funcJsonArray.optJSONObject(i).getString("FuncName").equals("FuncOutStorageA")==true){
                        Func_SingleScan("SingleOut","单包出仓");
                    }
                    if (funcJsonArray.optJSONObject(i).getString("FuncName").equals("FuncTransDepart")==true){
                        Func_SingleScan("TransDepart","转组出库");
                    }
                    if (funcJsonArray.optJSONObject(i).getString("FuncName").equals("FuncCartOut")==true){
                        Func_SingleScan("CartOut","推车出仓");
                    }
                    if (funcJsonArray.optJSONObject(i).getString("FuncName").equals("FuncInStorageB")==true){
                        Func_WholeScan("WholeIn","整单入仓");
                    }
                    if (funcJsonArray.optJSONObject(i).getString("FuncName").equals("FuncOutStorageB")==true){
                        Func_WholeScan("WholeOut","整单出仓");
                    }
                    if (funcJsonArray.optJSONObject(i).getString("FuncName").equals("FuncPartOut")==true){
                        Func_WholeScan("PartOut","部件出仓");
                    }
                    if (funcJsonArray.optJSONObject(i).getString("FuncName").equals("Cut2Templet")==true){
                        Func_WholeScan("Cut2Templet","裁床发模板");
                    }
                    if (funcJsonArray.optJSONObject(i).getString("FuncName").equals("Templet2Factory")==true){
                        Func_WholeScan("Templet2Factory","模板发车间");
                    }
                    if (funcJsonArray.optJSONObject(i).getString("FuncName").equals("FuncCheck9999")==true){
                        Func_CheckScan("9999","裁床验片");
                    }
                    if (funcJsonArray.optJSONObject(i).getString("FuncName").equals("FuncCheck9998")==true){
                        Func_CheckScan("9998","印花验片");
                    }
                    if (funcJsonArray.optJSONObject(i).getString("FuncName").equals("FuncCheck9997")==true){
                        Func_CheckScan("9997","绣花验片");
                    }
                    if (funcJsonArray.optJSONObject(i).getString("FuncName").equals("FuncCheck9996")==true){
                        Func_CheckScan("9996","特殊验片");
                    }
                    if (funcJsonArray.optJSONObject(i).getString("FuncName").equals("FuncCheck8888")==true){
                        Func_CheckScan("8888","后道收发");
                    }
                    if (funcJsonArray.optJSONObject(i).getString("FuncName").equals("FuncQueryTemplet")==true){
                        Func_QueryTemplet();
                    }
                    if (funcJsonArray.optJSONObject(i).getString("FuncName").equals("FuncQueryStorage")==true){
                        Func_Query();
                    }
                    if (funcJsonArray.optJSONObject(i).getString("FuncName").equals("FuncSplitPack")==true){
                        Func_Split();
                    }
                    if (funcJsonArray.optJSONObject(i).getString("FuncName").equals("FuncManualOperation")==true){
                        Func_Execute("ManualOperation","人工操作");
                    }
                    if (funcJsonArray.optJSONObject(i).getString("FuncName").equals("FuncTailorCall")==true){
                        Func_Execute("TailorCall","裁剪呼叫");
                    }
                    if (funcJsonArray.optJSONObject(i).getString("FuncName").equals("FuncNH")==true){
                        Func_Execute("NH","粘合呼叫");
                    }
                    if (funcJsonArray.optJSONObject(i).getString("FuncName").equals("FuncCR")==true){
                        Func_Execute("CR","充绒呼叫");
                    }
                    if (funcJsonArray.optJSONObject(i).getString("FuncName").equals("FuncCJLB")==true){
                        Func_Execute("FuncCJLB","裁剪拉布");
                    }
                    if (funcJsonArray.optJSONObject(i).getString("FuncName").equals("FuncYZSM")==true){
                        Func_Execute("FuncYZSM","验针扫描");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        txt_titleInv=findViewById(R.id.txt_titleInv);
        frameHintText=findViewById(R.id.frameHintText);
        btn_LoginClose=findViewById(R.id.btn_return);
        btn_LoginClose.setOnClickListener(this);


    }

    //调用功能A
    private void Func_Execute(String paramFormType, String paramCaption) {
        //简单参数通过Intent的Extras传递;
        Bundle paramBundle = new Bundle();
        //跳转前关闭键盘
        Intent aimerActivity=null;
        //裁剪呼叫
        if (paramFormType.equals("TailorCall")==true) {
            aimerActivity = new Intent(MainActivity.this, TailorCallActivity.class);
        }
        //验针扫描
        if (paramFormType.equals("FuncYZSM")==true) {
            aimerActivity = new Intent(MainActivity.this, TailorCallActivity.class);
        }
        if (paramFormType.equals("NH")==true) {
            aimerActivity = new Intent(MainActivity.this, NHActivity.class);
        }
        if (paramFormType.equals("CR")==true) {
            aimerActivity = new Intent(MainActivity.this, CRActivity.class);
        }
        if (paramFormType.equals("ManualOperation")==true) {
            aimerActivity = new Intent(MainActivity.this, ManualOperationActivity.class);
        }
        if (paramFormType.equals("FuncCJLB")==true) {
            aimerActivity = new Intent(MainActivity.this, CJLBActivity.class);
        }
        paramBundle.putString("LoginUserNo", curLoginUserNo);
        paramBundle.putString("LoginUser", curLoginUser);
        paramBundle.putString("LoginTime", curLoginTime);
        paramBundle.putString("serviceUrl", serviceUrl);
        paramBundle.putString("serviceUrlExt", serviceUrlExt);
        paramBundle.putString("FormType", paramFormType);
        paramBundle.putString("Caption", paramCaption);
        aimerActivity.putExtras(paramBundle);
        //启动主界面;
        startActivity(aimerActivity);
    }
    //调用功能A
    private void Func_CheckScan(String paramFormType, String paramCaption) {
        //跳转前关闭键盘
        Intent aimerActivity  = new Intent(MainActivity.this,CheckActivity.class);
        //简单参数通过Intent的Extras传递;
        Bundle paramBundle = new Bundle();
        paramBundle.putString("LoginUserNo", curLoginUserNo);
        paramBundle.putString("LoginUser", curLoginUser);
        paramBundle.putString("LoginTime", curLoginTime);
        paramBundle.putString("serviceUrl", serviceUrl);
        paramBundle.putString("serviceUrlExt", serviceUrlExt);
        paramBundle.putString("FormType", paramFormType);
        paramBundle.putString("Caption", paramCaption+"扫描");
        aimerActivity.putExtras(paramBundle);
        //启动主界面;
        startActivity(aimerActivity);
    }
    //调用功能A
    private void Func_SingleScan(String paramFormType, String paramCaption) {
        //跳转前关闭键盘
        Intent aimerActivity  = new Intent(MainActivity.this, StorageActivity.class);
        //简单参数通过Intent的Extras传递;
        Bundle paramBundle = new Bundle();
        paramBundle.putString("LoginUserNo", curLoginUserNo);
        paramBundle.putString("LoginUser", curLoginUser);
        paramBundle.putString("LoginTime", curLoginTime);
        paramBundle.putString("serviceUrl", serviceUrl);
        paramBundle.putString("serviceUrlExt", serviceUrlExt);
        paramBundle.putString("FormType", paramFormType);
        paramBundle.putString("Caption", paramCaption+"扫描");
        aimerActivity.putExtras(paramBundle);
        //启动主界面;
        startActivity(aimerActivity);
    }
    //调用功能A
    private void Func_WholeScan(String paramFormType, String paramCaption) {
        //跳转前关闭键盘
        Intent wholeActivity  = new Intent(MainActivity.this, WholeActivity.class);
        //简单参数通过Intent的Extras传递;
        Bundle paramBundle = new Bundle();
        paramBundle.putString("LoginUserNo", curLoginUserNo);
        paramBundle.putString("LoginUser", curLoginUser);
        paramBundle.putString("LoginTime", curLoginTime);
        paramBundle.putString("serviceUrl", serviceUrl);
        paramBundle.putString("serviceUrlExt", serviceUrlExt);
        paramBundle.putString("FormType", paramFormType);
        paramBundle.putString("Caption", paramCaption+"扫描");
        wholeActivity.putExtras(paramBundle);
        //启动主界面;
        startActivity(wholeActivity);
    }

    //调用功能A
    private void Func_Query() {
        //跳转前关闭键盘
        Intent queryActivity  = new Intent(MainActivity.this, QueryActivity.class);
        //简单参数通过Intent的Extras传递;
        Bundle paramBundle = new Bundle();
        paramBundle.putString("LoginUserNo", curLoginUserNo);
        paramBundle.putString("LoginUser", curLoginUser);
        paramBundle.putString("LoginTime", curLoginTime);
        paramBundle.putString("serviceUrl", serviceUrl);
        paramBundle.putString("serviceUrlExt", serviceUrlExt);
        paramBundle.putString("FormType", "Query");
        paramBundle.putString("Caption", "裁片库存查询");
        queryActivity.putExtras(paramBundle);
        //启动主界面;
        startActivity(queryActivity);
    }
    //调用功能A
    private void Func_QueryTemplet() {
        //跳转前关闭键盘
        Intent queryActivity  = new Intent(MainActivity.this, TempActivity.class);
        //简单参数通过Intent的Extras传递;
        Bundle paramBundle = new Bundle();
        paramBundle.putString("LoginUserNo", curLoginUserNo);
        paramBundle.putString("LoginUser", curLoginUser);
        paramBundle.putString("LoginTime", curLoginTime);
        paramBundle.putString("serviceUrl", serviceUrl);
        paramBundle.putString("serviceUrlExt", serviceUrlExt);
        paramBundle.putString("FormType", "QueryTemplet");
        paramBundle.putString("Caption", "模板库存查询");
        queryActivity.putExtras(paramBundle);
        //启动主界面;
        startActivity(queryActivity);
    }

    private void Func_Split() {
        //跳转前关闭键盘
        Intent queryActivity  = new Intent(MainActivity.this, SplitActivity.class);
        //简单参数通过Intent的Extras传递;
        Bundle paramBundle = new Bundle();
        paramBundle.putString("LoginUserNo", curLoginUserNo);
        paramBundle.putString("LoginUser", curLoginUser);
        paramBundle.putString("LoginTime", curLoginTime);
        paramBundle.putString("serviceUrl", serviceUrl);
        paramBundle.putString("serviceUrlExt", serviceUrlExt);
        paramBundle.putString("FormType", "Query");
        paramBundle.putString("Caption", "裁片拆分");
        queryActivity.putExtras(paramBundle);
        //启动主界面;
        startActivity(queryActivity);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_return:
                onBackPressed();

                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}
