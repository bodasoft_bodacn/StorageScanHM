package com.bodacn.storagescan.bean;

public class PlaceBean {
    String OperType;
    String RowNo;
    String ToPlaceNo;

    public String getOperType() {
        return OperType;
    }

    public void setOperType(String operType) {
        OperType = operType;
    }

    public String getRowNo() {
        return RowNo;
    }

    public void setRowNo(String rowNo) {
        RowNo = rowNo;
    }

    public String getToPlaceNo() {
        return ToPlaceNo;
    }

    public void setToPlaceNo(String toPlaceNo) {
        ToPlaceNo = toPlaceNo;
    }
}
