package com.bodacn.storagescan.bean;

public class CallAgvInfo {
    String OperType;
    String ReqCode;
    String StartPlace;
    String EndPlace;
    String RowIndex;

    public String getOperType() {
        return OperType;
    }

    public void setOperType(String operType) {
        OperType = operType;
    }

    public String getReqCode() {
        return ReqCode;
    }

    public void setReqCode(String reqCode) {
        ReqCode = reqCode;
    }

    public String getStartPlace() {
        return StartPlace;
    }

    public void setStartPlace(String startPlace) {
        StartPlace = startPlace;
    }

    public String getEndPlace() {
        return EndPlace;
    }

    public void setEndPlace(String endPlace) {
        EndPlace = endPlace;
    }

    public String getRowIndex() {
        return RowIndex;
    }

    public void setRowIndex(String rowIndex) {
        RowIndex = rowIndex;
    }
}
