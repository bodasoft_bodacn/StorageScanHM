package com.bodacn.storagescan.bean;

public class BindOrUnBindBean {
    String OperType;
    String ReqCode;
    String podCode;
    String positionCode;
    String indBind;

    public String getOperType() {
        return OperType;
    }

    public void setOperType(String operType) {
        OperType = operType;
    }

    public String getReqCode() {
        return ReqCode;
    }

    public void setReqCode(String reqCode) {
        ReqCode = reqCode;
    }

    public String getPodCode() {
        return podCode;
    }

    public void setPodCode(String podCode) {
        this.podCode = podCode;
    }

    public String getPositionCode() {
        return positionCode;
    }

    public void setPositionCode(String positionCode) {
        this.positionCode = positionCode;
    }

    public String getIndBind() {
        return indBind;
    }

    public void setIndBind(String indBind) {
        this.indBind = indBind;
    }
}
