package com.bodacn.storagescan.bean;

public class ProcessDetailBean {
    String OperType;
    String PlaceNo;
    String AGVRackMxID;
    String RackNo;
    String FabricNo;
    String UpdateTime;
    String LooseRackNo;
    String TXM;
    String OrderNo;
    String StyleNo;
    String Color;
    String Size;
    String Number;
    String PartName;
    String ResultInfo;
    String memo;

    public String getResultInfo() {
        return ResultInfo;
    }

    public void setResultInfo(String resultInfo) {
        ResultInfo = resultInfo;
    }

    public String getOperType() {
        return OperType;
    }

    public void setOperType(String operType) {
        OperType = operType;
    }

    public String getPlaceNo() {
        return PlaceNo;
    }

    public String getAGVRackMxID() {
        return AGVRackMxID;
    }

    public void setAGVRackMxID(String AGVRackMxID) {
        this.AGVRackMxID = AGVRackMxID;
    }

    public String getRockNo() {
        return RackNo;
    }

    public void setRockNo(String rockNo) {
        RackNo = rockNo;
    }

    public String getFabricNo() {
        return FabricNo;
    }

    public void setFabricNo(String fabricNo) {
        FabricNo = fabricNo;
    }

    public String getUpdateTime() {
        return UpdateTime;
    }

    public void setUpdateTime(String updateTime) {
        UpdateTime = updateTime;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public void setPlaceNo(String placeNo) {
        PlaceNo = placeNo;
    }

    public String getLooseRackNo() {
        return LooseRackNo;
    }

    public void setLooseRackNo(String looseRackNo) {
        LooseRackNo = looseRackNo;
    }

    public String getTXM() {
        return TXM;
    }

    public void setTXM(String TXM) {
        this.TXM = TXM;
    }

    public String getOrderNo() {
        return OrderNo;
    }

    public void setOrderNo(String orderNo) {
        OrderNo = orderNo;
    }

    public String getStyleNo() {
        return StyleNo;
    }

    public void setStyleNo(String styleNo) {
        StyleNo = styleNo;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String color) {
        Color = color;
    }

    public String getSize() {
        return Size;
    }

    public void setSize(String size) {
        Size = size;
    }

    public String getNumber() {
        return Number;
    }

    public void setNumber(String number) {
        Number = number;
    }

    public String getPartName() {
        return PartName;
    }

    public void setPartName(String partName) {
        PartName = partName;
    }
}
