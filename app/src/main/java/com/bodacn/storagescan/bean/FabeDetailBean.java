package com.bodacn.storagescan.bean;

public class FabeDetailBean {
    String OperType;
    String PlaceNo;
    String LooseRackNo;
    String FabricNo;
    String OrderNo;
    String BaleName;
    String VatNo;
    String VolumeNo;
    String songbuTime;

    public String getSongbuTime() {
        return songbuTime;
    }

    public void setSongbuTime(String songbuTime) {
        songbuTime = songbuTime;
    }

    public String getOperType() {
        return OperType;
    }

    public void setOperType(String operType) {
        OperType = operType;
    }

    public String getPlaceNo() {
        return PlaceNo;
    }

    public void setPlaceNo(String placeNo) {
        PlaceNo = placeNo;
    }

    public String getLooseRackNo() {
        return LooseRackNo;
    }

    public void setLooseRackNo(String looseRackNo) {
        LooseRackNo = looseRackNo;
    }

    public String getFabricNo() {
        return FabricNo;
    }

    public void setFabricNo(String fabricNo) {
        FabricNo = fabricNo;
    }

    public String getOrderNo() {
        return OrderNo;
    }

    public void setOrderNo(String orderNo) {
        OrderNo = orderNo;
    }

    public String getBaleName() {
        return BaleName;
    }

    public void setBaleName(String baleName) {
        BaleName = baleName;
    }

    public String getVatNo() {
        return VatNo;
    }

    public void setVatNo(String vatNo) {
        VatNo = vatNo;
    }

    public String getVolumeNo() {
        return VolumeNo;
    }

    public void setVolumeNo(String volumeNo) {
        VolumeNo = volumeNo;
    }
}
