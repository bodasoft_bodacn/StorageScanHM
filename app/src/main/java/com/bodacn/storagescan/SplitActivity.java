package com.bodacn.storagescan;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.widget.PullRefreshLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;

public class SplitActivity extends AppCompatActivity implements View.OnClickListener {
    private String TAG = this.getClass().getName();
    private final static int MSG_AllowScan=9000;
    private final static int MSG_ScanBarcodeOver=9001;
    private final static int MSG_Detail =9002;
    TextView txt_titleInv, tv_barcodeCaption,tv_ResultInfo;
    LinearLayout liner_title;
    Button btn_Query;
    ImageButton btn_return;
    EditText edit_Barcode;
    String serviceUrl="http://192.168.10.12/tvservice";
    String serviceUrlExt  = "/BDService.asmx?wsdl";
    String curLoginUserNo,curLoginUser,curLoginTime;//操作员所属部门等信息;
    String FormType="",Caption="",PrBarcode="",PrHouseDeID="",PrPartName="",PrNumber="";
    JSONArray detailJSON;
    int pageItemCount=3;
    boolean ScanSign=false;
    ListView list_Order;
    Button btn_refreshDetail;
    String SizeArray[]={"S","M","L","2XL","3XL","4XL"};
    //拦截扫描枪输入部分
    String BarcodeStr="";
    ScannerManager scannerManager;
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (scannerManager.dispatchKeyEvent(event)) {
            return true;
        }
        return super.dispatchKeyEvent(event);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_split);
        InitView();
        //获取登陆参数
        Bundle paramBundle = this.getIntent().getExtras();
        curLoginUserNo=paramBundle.getString("LoginUserNo");
        curLoginUser=paramBundle.getString("LoginUser");
        curLoginTime=paramBundle.getString("LoginTime");
        serviceUrl=paramBundle.getString("serviceUrl");
        serviceUrlExt=paramBundle.getString("serviceUrlExt");
        FormType=paramBundle.getString("FormType");
        Caption=paramBundle.getString("Caption");
        //置标题颜色
        txt_titleInv.setText(Caption);
        //拦截扫描事件
        scannerManager = new ScannerManager(new ScannerManager.OnScanListener() {
            @Override
            public void onResult(String code) {
                Log.d(TAG, "code= " + code);
                ((MainApplication)getApplication()).playRingtone();
                edit_Barcode.setText(code);
                ScanBarcode(edit_Barcode.getText().toString());
            }
        });
    }


    //判断是否为数字，不然不转
    public static boolean isNumer(String str) {
        try {
            new BigDecimal(str);
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    private void InitView(){
        list_Order=findViewById(R.id.list_Order);
        list_Order.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            }
        });
        btn_Query=findViewById(R.id.btn_Scan);
        btn_Query.setOnClickListener(this);
        tv_ResultInfo=findViewById(R.id.tv_ResultInfo);
        btn_refreshDetail=findViewById(R.id.btn_refreshDetail);
        btn_refreshDetail.setOnClickListener(this);
        edit_Barcode=findViewById(R.id.edit_Barcode);
        edit_Barcode.setInputType(InputType.TYPE_NULL);
        edit_Barcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edit_Barcode.setInputType(InputType.TYPE_CLASS_TEXT);
                //1.得到InputMethodManager对象
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                //2.调用showSoftInput方法显示软键盘，其中view为聚焦的view组件
                imm.showSoftInput(edit_Barcode,InputMethodManager.SHOW_FORCED);
            }
        });
        edit_Barcode.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event!=null){
                    //Log.e("Debug","-----------------------------Debug-----------------------------");
                    //System.out.println(event);
                    if ((event.getAction()==KeyEvent.ACTION_UP)&&(event.getKeyCode()== KeyEvent.KEYCODE_ENTER))
                    {
                        //虚拟键盘或实体键盘可考虑是否通过deviceID，或source来判断
                        // KeyEvent:KeyEvent { action=ACTION_DOWN, keyCode=KEYCODE_ENTER, scanCode=0, metaState=0, flags=0x16, repeatCount=0, eventTime=142515176, downTime=142515176, deviceId=-1, source=0x0 }
                        //隐藏键盘
                        //System.out.println(edit_Barcode.getText().toString());
                        ScanBarcode(edit_Barcode.getText().toString());
                        return true;
                    }
                }
                return false;
            }
        });
        edit_Barcode.requestFocus();
        edit_Barcode.requestFocusFromTouch();
        liner_title=findViewById(R.id.liner_title);
        tv_barcodeCaption =findViewById(R.id.tv_barcodeCaption);
        txt_titleInv=findViewById(R.id.txt_titleInv);
        btn_return =findViewById(R.id.btn_return);
        btn_return.setOnClickListener(this);
        ((Button)findViewById(R.id.btnSplitAdd)).setOnClickListener(this);
        ((Button)findViewById(R.id.btnSplitClear)).setOnClickListener(this);
        ((Button)findViewById(R.id.btnSplitSave)).setOnClickListener(this);

    }

    // 隐藏手机键盘
    private void hideIM(View edt) {
        try {
            InputMethodManager im = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);
            IBinder windowToken = edt.getWindowToken();
            if (windowToken != null) {
                im.hideSoftInputFromWindow(windowToken, 0);
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void onClick(View view) {
        hideIM(view);
        switch (view.getId()){
            case R.id.btnSplitAdd:
                if (PrBarcode.length()>0) showEditPakcNum(PrNumber,((EditText)findViewById(R.id.edt_Size)).getText().toString());
                else {
                    Toast.makeText(this,"请先扫描包条码!",Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btnSplitClear:
                //mediaPlayer.start();
                AlertDialog alertDialog = new AlertDialog.Builder(this)
                        .setTitle(getString(R.string.SystemHint)).setMessage("您确定要清除所有拆分记录吗？")
                        .setPositiveButton(getString(R.string.Affirm), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                hideIM(edit_Barcode);
                                edit_Barcode.selectAll();
                                edit_Barcode.requestFocus();
                                try {
                                    detailJSON=new JSONArray("[]");
                                    //刷新表格
                                    DetailGridAdapter detailGridAdapter = new DetailGridAdapter(getApplicationContext());
                                    detailGridAdapter.jsonArray = detailJSON;
                                    list_Order.setAdapter(detailGridAdapter);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                return;
                            }
                        }).setNeutralButton("取消", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        }).create(); // 创建对话框
                alertDialog.show(); // 显示对话框
                break;
            case R.id.btnSplitSave:
                if (detailJSON.length()>0) SaveSplit(((TextView)findViewById(R.id.txt_PieceValue)).getText().toString());
                else {
                    Toast.makeText(this,"请先增加拆分方法!",Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btn_return:
                finish();//关闭当前Activity，返回上一级;
                break;
            case R.id.btn_Scan:
                if ((edit_Barcode.getText()!=null)&&(edit_Barcode.getText().length()>0)) {
                    ScanBarcode(edit_Barcode.getText().toString());
                } else Toast.makeText(getApplicationContext(),"请先输入条码!",Toast.LENGTH_LONG).show();
                break;
            default:
                break;
        }
    }

    private void SaveSplit(String paramNumber) {
        //计算件数
        int totalNum=0;
        String SplitStr="",SizeStr="";

        if ((detailJSON!=null)&&(detailJSON.length()>0)){
            for (int i=0;i<detailJSON.length();i++){
                if (detailJSON.optJSONObject(i).has("TotalNum")){
                    try {
                        if (isNumer(detailJSON.optJSONObject(i).getString("TotalNum"))){
                            totalNum=totalNum+Integer.parseInt(detailJSON.optJSONObject(i).getString("TotalNum"));
                            if (SplitStr.length()==0) SplitStr=detailJSON.optJSONObject(i).getString("TotalNum");
                            else SplitStr=SplitStr+";"+detailJSON.optJSONObject(i).getString("TotalNum");
                            if (SizeStr.length()==0) SizeStr=detailJSON.optJSONObject(i).getString("Size");
                            else SizeStr=SizeStr+";"+detailJSON.optJSONObject(i).getString("Size");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (paramNumber.equals(String.valueOf(totalNum))){
                String SqlComm="Exec App_SplitBarcode @Barcode='"+PrBarcode+"',@FormType='"+FormType+"',@PrBarcode='"+PrBarcode+"',@SplitStr='"+SplitStr+"',@SizeStr='"+SizeStr
                        +"',@OperType='SplitNum',@UserNo='" + curLoginUserNo+"'";
                //调用WebService过程并处理界面更新;
                ReadSQLData(SqlComm);

            } else
            {
                Toast.makeText(getApplicationContext(),"件数不符不能保存拆分记录!",Toast.LENGTH_LONG).show();
            }
        }

    }



    //修改包件数;
    private void showEditPakcNum(String paramNum ,String paramSize){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("件数更改");
        //    通过LayoutInflater来加载一个xml的布局文件作为一个View对象
        View view = LayoutInflater.from(this).inflate(R.layout.input_num, null);
        //    设置我们自己定义的布局文件作为弹出框的Content
        builder.setView(view);
        final EditText edt_oldNum =view.findViewById(R.id.edt_oldNum);
        edt_oldNum.setText(paramNum);
        final EditText edt_newNum = view.findViewById(R.id.edt_newNum);
        final Spinner edt_newSize = view.findViewById(R.id.edt_newSize);
        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,android.R.id.text1,SizeArray);
        edt_newSize.setAdapter(adapter);
        for (int i=0;i<SizeArray.length;i++){
            if (SizeArray[i].equals(paramSize)){
                edt_newSize.setSelection(i);
                break;
            }
        }
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                String a = edt_oldNum.getText().toString().trim();
                String b = edt_newNum.getText().toString().trim();
                String size = edt_newSize.getSelectedItem().toString();
                if (b.length()>0) editPackNo(PrBarcode,b,size);
            }
        });
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {

            }
        });
        builder.show();
    }
    //刷新缸布列表;
    private void editPackNo(String BarcodeParam,String NumberParam,String SizeParam) {
        if (detailJSON==null) {
           try {
               detailJSON=new JSONArray("[]");
           } catch (JSONException e) {
               e.printStackTrace();
           }
        }
        try {
            JSONObject tmpJson=new JSONObject();
            tmpJson.put("SerNo",detailJSON.length()+1);
            tmpJson.put("PartName",((EditText)findViewById(R.id.edt_PartName)).getText().toString());
            tmpJson.put("Color",((EditText)findViewById(R.id.edt_Color)).getText().toString());
            tmpJson.put("Size",SizeParam);
            tmpJson.put("TotalNum",NumberParam);
            detailJSON.put(tmpJson);
            //刷新表格
            DetailGridAdapter detailGridAdapter = new DetailGridAdapter(getApplicationContext());
            detailGridAdapter.jsonArray = detailJSON;
            list_Order.setAdapter(detailGridAdapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //登陆校验;
    private void ScanBarcode(String paramBarcode){
        hideIM(edit_Barcode);
        //组织WebService地址;
        edit_Barcode.setEnabled(false);
        edit_Barcode.setText("");
        //刷新表格
        try {
            detailJSON=new JSONArray("[]");
            DetailGridAdapter detailGridAdapter = new DetailGridAdapter(getApplicationContext());
            detailGridAdapter.jsonArray = detailJSON;
            list_Order.setAdapter(detailGridAdapter);
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
        String SqlComm="Exec App_SplitBarcode @Barcode='"+paramBarcode+"',@FormType='"+FormType+"',@PrBarcode='"+(PrBarcode)
                +"',@OperType='ScanBarcode',@UserNo='" + curLoginUserNo+"'";
        PrBarcode=paramBarcode;
        //调用WebService过程并处理界面更新;
        ReadSQLData(SqlComm);
    }



    //通过消息来执行处理类程序,不更新UI则不一定需要通过这个;
    final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            // TODO Auto-generated method stub
            // 根据消息ID来判断要做的事情
            int msgId = msg.what;
            hideIM(edit_Barcode);
            edit_Barcode.setEnabled(true);
            edit_Barcode.selectAll();
            edit_Barcode.requestFocus();
            ScanSign=false;
            switch (msgId) {
                case MSG_Detail:
                    if (detailJSON!=null) {
                        //刷新表格
                        DetailGridAdapter detailGridAdapter = new DetailGridAdapter(getApplicationContext());
                        detailGridAdapter.jsonArray = detailJSON;
                        list_Order.setAdapter(detailGridAdapter);
                    }
                    break;
                case MSG_ScanBarcodeOver:
                    if (detailJSON!=null) {
                        //填充屏幕
                        fillScreen(detailJSON.optJSONObject(0));
                    }
                    break;
                case MSG_AllowScan:
                    ScanSign=false;
                    break;
                default:
                    break;
            }
        }
    };

    private void fillScreen(JSONObject optJSONObject) {
        if (optJSONObject!=null){
            try {
                if (optJSONObject.has("OrderNo")) ((EditText) findViewById(R.id.edt_OrderNo)).setText(optJSONObject.getString("OrderNo"));
                if (optJSONObject.has("StyleNo")) ((EditText) findViewById(R.id.edt_StyleNo)).setText(optJSONObject.getString("StyleNo"));
                if (optJSONObject.has("Size")) ((EditText) findViewById(R.id.edt_Size)).setText(optJSONObject.getString("Size"));
                if (optJSONObject.has("Color")) ((EditText) findViewById(R.id.edt_Color)).setText(optJSONObject.getString("Color"));
                if (optJSONObject.has("HouseDeName")) ((EditText) findViewById(R.id.edt_HouseDeName)).setText(optJSONObject.getString("HouseDeName"));
                if (optJSONObject.has("HouseDeID")) PrHouseDeID=optJSONObject.getString("HouseDeID");
                if (optJSONObject.has("CurPackNum")) PrNumber=optJSONObject.getString("CurPackNum");
                if (optJSONObject.has("SizeArray")){
                    String tmpStr=optJSONObject.getString("SizeArray");
                    SizeArray = tmpStr.split(",");
                }
                if (optJSONObject.has("PartName")){
                    PrPartName=optJSONObject.getString("PartName");
                    ((EditText) findViewById(R.id.edt_PartName)).setText(PrPartName);
                }
                if (optJSONObject.has("PackValue")) ((TextView) findViewById(R.id.txt_PackValue)).setText(optJSONObject.getString("PackValue"));
                if (optJSONObject.has("PieceValue")) ((TextView) findViewById(R.id.txt_PieceValue)).setText(optJSONObject.getString("PieceValue"));
                if (optJSONObject.has("ResultInfo")) ((TextView) findViewById(R.id.tv_ResultInfo)).setText(optJSONObject.getString("ResultInfo"));
                if (optJSONObject.has("ErrInfo") == true) {
                    //MediaPlayer mediaPlayer = MediaPlayer.create(getActivity(), R.raw.warn);
                    //mediaPlayer.start();
                    AlertDialog alertDialog = new AlertDialog.Builder(this)
                            .setTitle(getString(R.string.SystemHint)).setMessage(optJSONObject.getString("ErrInfo"))
                            .setPositiveButton(getString(R.string.Affirm), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    hideIM(edit_Barcode);
                                    edit_Barcode.selectAll();
                                    edit_Barcode.requestFocus();
                                    return;
                                }
                            }).create(); // 创建对话框
                    alertDialog.show(); // 显示对话框
                }
                if (optJSONObject.has("HaveDetail")){
                    if (optJSONObject.getString("HaveDetail").equals("Y")) {
                        //刷新表格
                        DetailGridAdapter detailGridAdapter = new DetailGridAdapter(getApplicationContext());
                        detailGridAdapter.jsonArray = detailJSON;
                        list_Order.setAdapter(detailGridAdapter);
                    } else detailJSON=new JSONArray("[]");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    //直接连获取数据，与Handler配套使用;
    private void ReadSQLData(final String SQLComm) {
        ScanSign=true;
        System.out.println("SQLComm:"+SQLComm);
        Runnable run = new Runnable() {
            @Override
            public void run() {
                try{
                    JSONArray jsonArray  = DBUtil.QuerySQL2JSON(SQLComm);
                    System.out.println(jsonArray);
                    ScanSign=false;
                    //往界面转数据;
                    if ((jsonArray!=null)&&(jsonArray.length()>0)) {
                        if (jsonArray.optJSONObject(0).has("OperType")){
                            if (jsonArray.optJSONObject(0).getString("OperType").equals("ScanBarcode")==true){
                                detailJSON=jsonArray;
                                //刷新界面;
                                Message msg = new Message();
                                msg.what = MSG_ScanBarcodeOver;
                                handler.removeMessages(msg.what);
                                handler.sendMessage(msg);
                            }
                            if (jsonArray.optJSONObject(0).getString("OperType").equals("ScanDetail")==true){
                                detailJSON=jsonArray;
                                //刷新界面;
                                Message msg = new Message();
                                msg.what = MSG_Detail;
                                handler.removeMessages(msg.what);
                                handler.sendMessage(msg);
                            }
                            if (jsonArray.optJSONObject(0).getString("OperType").equals("SplitNum")==true){
                                detailJSON=jsonArray;
                                //刷新界面;
                                Message msg = new Message();
                                msg.what = MSG_ScanBarcodeOver;
                                handler.removeMessages(msg.what);
                                handler.sendMessage(msg);
                            }
                        }
                    } else{
                        Message msg = new Message();
                        msg.what = MSG_AllowScan;
                        handler.removeMessages(msg.what);
                        handler.sendMessage(msg);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    //刷新界面;
                    ScanSign=false;
                    Message msg = new Message();
                    msg.what = MSG_AllowScan;
                    handler.removeMessages(msg.what);
                    handler.sendMessage(msg);
                }

            }

        };
        new Thread(run).start();

    }



    //订单表格适配器;
    public final class DetailGridAdapter extends BaseAdapter {
        private LayoutInflater inflater;
        public DetailGridAdapter(Context context) {
            inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        private JSONArray jsonArray;

        @Override
        public int getCount() {
            return jsonArray.length();
        }

        @Override
        public Object getItem(int position) {
            try {
                return jsonArray.get(position);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) convertView = inflater.inflate(R.layout.activity_split_item, null, false);
            String tmpValue;
            int pullVatListWidth=parent.getWidth();
            convertView.setLayoutParams(new ListView.LayoutParams((int)pullVatListWidth,(int)(parent.getHeight()/pageItemCount)));
            int backcolor=getResources().getColor(R.color.white);
            int fontcolor=getResources().getColor(R.color.text_black);
            if (position%2==1) backcolor=getResources().getColor(R.color.listContentColor);
            try {
                if (jsonArray.optJSONObject(position).has("ConColor")==true) {
                    tmpValue = jsonArray.optJSONObject(position).getString("ConColor");
                    backcolor=GetColorValue(tmpValue);
                }
                if (jsonArray.optJSONObject(position).has("ConFontColor")==true) {
                    tmpValue = jsonArray.optJSONObject(position).getString("ConFontColor");
                    fontcolor=GetColorValue(tmpValue);
                }
                TextView tv = convertView.findViewById(R.id.tv_PartName);// 显示文字
                if (jsonArray.optJSONObject(position).has("PartName")==true) {
                    tv.setText(jsonArray.optJSONObject(position).getString("PartName"));
                }
                tv.setTextColor(fontcolor);
                tv.setBackgroundColor(backcolor);
                tv = convertView.findViewById(R.id.tv_SerNo);// 显示文字
                if (jsonArray.optJSONObject(position).has("SerNo")==true) {
                    tv.setText(jsonArray.optJSONObject(position).getString("SerNo"));
                }
                tv.setTextColor(fontcolor);
                tv.setBackgroundColor(backcolor);
                tv = convertView.findViewById(R.id.tv_Color);// 显示文字
                if (jsonArray.optJSONObject(position).has("Color")==true)
                    tv.setText(jsonArray.optJSONObject(position).getString("Color"));
                tv.setTextColor(fontcolor);
                tv.setBackgroundColor(backcolor);
                tv = convertView.findViewById(R.id.tv_Size);// 显示文字
                if (jsonArray.optJSONObject(position).has("Size")==true)
                    tv.setText(jsonArray.optJSONObject(position).getString("Size"));
                tv.setTextColor(fontcolor);
                tv.setBackgroundColor(backcolor);
                tv = convertView.findViewById(R.id.tv_TotalNum);// 显示文字
                if (jsonArray.optJSONObject(position).has("TotalNum")==true)
                    tv.setText(jsonArray.optJSONObject(position).getString("TotalNum"));
                tv.setTextColor(fontcolor);
                tv.setBackgroundColor(backcolor);
            } catch (JSONException e) {
                e.printStackTrace();
                System.out.println(e.getMessage());
            }
            Log.d("msg","DetailGridAdapter_position:"+position);
            return convertView;
        }

    }



    //从字符串转颜色
    private int GetColorValue(String paramColor){
        int PenColor= Color.WHITE;
        String StrPenColor=paramColor;
        if ((StrPenColor.indexOf("#")==0)&&(StrPenColor.length()==7)){
            int testColor=Integer.parseInt(StrPenColor.substring(1),16);
            int red = (testColor & 0xff0000) >> 16;
            int green = (testColor & 0x00ff00) >> 8;
            int blue = (testColor & 0x0000ff);
            PenColor=Color.rgb(red, green, blue);
        }else {
            if (isNumer(StrPenColor)) {
                int testColor = Integer.parseInt(StrPenColor);
                int red = (testColor & 0xff0000) >> 16;
                int green = (testColor & 0x00ff00) >> 8;
                int blue = (testColor & 0x0000ff);
                PenColor = Color.rgb(red, green, blue);
            } else {
                if (StrPenColor.equals("Black") == true)
                    PenColor = Color.BLACK;
                if (StrPenColor.equals("Blue") == true)
                    PenColor = Color.BLUE;
                if (StrPenColor.equals("Yellow") == true)
                    PenColor = Color.YELLOW;
                if (StrPenColor.equals("Red") == true)
                    PenColor = Color.RED;
                if (StrPenColor.equals("Green") == true)
                    PenColor = Color.GREEN;
                if (StrPenColor.equals("Gray") == true)
                    PenColor = Color.LTGRAY;
                if (StrPenColor.equals("White") == true)
                    PenColor = Color.WHITE;
            }
        }
        return PenColor;
    }



}
