package com.bodacn.storagescan.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bodacn.storagescan.R;
import com.bodacn.storagescan.bean.FabeDetailBean;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ZHOU on 2017/6/27.
 */

public class PieceBindAdapter extends BaseAdapter {

    private Context mContext;
    private List<FabeDetailBean> mLimitedList;
    private GridView mGv;


    public PieceBindAdapter(Context context, List<FabeDetailBean> mLimitedList) {
        this.mContext = context;
        this.mLimitedList = mLimitedList == null ? new ArrayList<FabeDetailBean>(0) : mLimitedList;
    }

    @Override
    public int getCount() {
        return mLimitedList.size();
    }

    @Override
    public Object getItem(int position) {
        return mLimitedList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder ;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.intem_on_piece_bind_detail, parent, false);
            holder=new ViewHolder(convertView);
            convertView.setTag(holder);
        }else {
            holder= (ViewHolder) convertView.getTag();
        }
        FabeDetailBean fabeDetailBean = mLimitedList.get(position);

        holder.tvKuweihao.setText(fabeDetailBean.getPlaceNo());
        holder.tvSongbujiahao.setText(fabeDetailBean.getLooseRackNo());
        holder.tvHetongaho.setText(fabeDetailBean.getOrderNo());
        holder.tvMianlaiomingcheng.setText(fabeDetailBean.getBaleName());
        holder.tvGanghao.setText(fabeDetailBean.getVatNo());
        holder.tvPihao.setText(fabeDetailBean.getVolumeNo());
        holder.tvMianliaotiaoma.setText(fabeDetailBean.getFabricNo());
        return convertView;
    }


    static
    class ViewHolder {
        @BindView(R.id.tv_kuweihao)
        TextView tvKuweihao;
        @BindView(R.id.tv_songbujiahao)
        TextView tvSongbujiahao;
        @BindView(R.id.tv_mianliaotiaoma)
        TextView tvMianliaotiaoma;
        @BindView(R.id.tv_hetongaho)
        TextView tvHetongaho;
        @BindView(R.id.tv_mianlaiomingcheng)
        TextView tvMianlaiomingcheng;
        @BindView(R.id.tv_ganghao)
        TextView tvGanghao;
        @BindView(R.id.tv_pihao)
        TextView tvPihao;
        @BindView(R.id.tv_songbushijian)
        TextView tvSongbushijian;
        @BindView(R.id.ll)
        LinearLayout ll;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}

