package com.bodacn.storagescan.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bodacn.storagescan.R;
import com.bodacn.storagescan.bean.ProcessDetailBean;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ZHOU on 2017/6/27.
 */

public class PiecelvScanAdapter extends BaseAdapter {

    private Context mContext;
    private List<ProcessDetailBean> mLimitedList;
    private GridView mGv;


    public PiecelvScanAdapter(Context context, List<ProcessDetailBean> mLimitedList) {
        this.mContext = context;
        this.mLimitedList = mLimitedList == null ? new ArrayList<ProcessDetailBean>(0) : mLimitedList;
    }

    @Override
    public int getCount() {
        return mLimitedList.size();
    }

    @Override
    public Object getItem(int position) {
        return mLimitedList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.intem_on_piece_scan_lv_detail, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        ProcessDetailBean processDetailBean = mLimitedList.get(position);
        holder.tvKuweihao.setText(processDetailBean.getPlaceNo());
        holder.tvHuojiahao.setText(processDetailBean.getRockNo());
        holder.tvBaotiaoma.setText(processDetailBean.getTXM());
        holder.tvHetongaho.setText(processDetailBean.getOrderNo());
        holder.tvKuanhao.setText(processDetailBean.getStyleNo());
        holder.tvYanse.setText(processDetailBean.getColor());
        holder.tvChima.setText(processDetailBean.getSize());
        holder.tvJianshu.setText(processDetailBean.getNumber());
        holder.tv_part.setText(processDetailBean.getPartName());
        return convertView;
    }


    class ViewHolder {
        @BindView(R.id.tv_kuweihao)
        TextView tvKuweihao;
        @BindView(R.id.tv_huojiahao)
        TextView tvHuojiahao;
        @BindView(R.id.tv_baotiaoma)
        TextView tvBaotiaoma;
        @BindView(R.id.tv_hetongaho)
        TextView tvHetongaho;
        @BindView(R.id.tv_kuanhao)
        TextView tvKuanhao;
        @BindView(R.id.tv_yanse)
        TextView tvYanse;
        @BindView(R.id.tv_chima)
        TextView tvChima;
        @BindView(R.id.tv_jianshu)
        TextView tvJianshu;
        @BindView(R.id.tv_part)
        TextView tv_part;

        @BindView(R.id.ll)
        LinearLayout ll;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}

