package com.bodacn.storagescan.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bodacn.storagescan.R;
import com.bodacn.storagescan.bean.PlaceBean;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ZHOU on 2017/6/27.
 */

public class PieceScanAdapter extends BaseAdapter {

    private Context mContext;
    private List<PlaceBean> mLimitedList;


    public PieceScanAdapter(Context context, List<PlaceBean> mLimitedList) {
        this.mContext = context;
        this.mLimitedList = mLimitedList == null ? new ArrayList<PlaceBean>(0) : mLimitedList;
    }

    @Override
    public int getCount() {
        return mLimitedList.size();
    }

    @Override
    public Object getItem(int position) {
        return mLimitedList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.intem_on_piece_scan_detail, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.tvKuweihao.setText(mLimitedList.get(position).getToPlaceNo());
        return convertView;
    }
    class ViewHolder {
        @BindView(R.id.tv_kuweihao)
        TextView tvKuweihao;
        @BindView(R.id.ll)
        LinearLayout ll;
        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}

