package com.bodacn.storagescan;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;

public class TempActivity extends AppCompatActivity implements View.OnClickListener {
    private String TAG = this.getClass().getName();
    private final static int MSG_AllowScan=9000;
    private final static int MSG_ScanBarcodeOver=9001;
    private final static int MSG_Detail =9002;
    TextView txt_titleInv, tv_barcodeCaption,tv_ResultInfo;
    LinearLayout liner_title;
    Button btn_Query;
    ImageButton btn_return;
    EditText edt_StyleNo;
    String serviceUrl="http://192.168.10.12/tvservice";
    String serviceUrlExt  = "/BDService.asmx?wsdl";
    String curLoginUserNo,curLoginUser,curLoginTime;//操作员所属部门等信息;
    String FormType,Caption,PrBarcode;
    JSONArray detailJSON;
    int pageItemCount=15;
    boolean ScanSign=false;
    ListView list_Order;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temp);
        InitView();
        //获取登陆参数
        Bundle paramBundle = this.getIntent().getExtras();
        curLoginUserNo=paramBundle.getString("LoginUserNo");
        curLoginUser=paramBundle.getString("LoginUser");
        curLoginTime=paramBundle.getString("LoginTime");
        serviceUrl=paramBundle.getString("serviceUrl");
        serviceUrlExt=paramBundle.getString("serviceUrlExt");
        FormType=paramBundle.getString("FormType");
        Caption=paramBundle.getString("Caption");
        //置标题颜色
        txt_titleInv.setText(Caption);

    }


    //判断是否为数字，不然不转
    public static boolean isNumer(String str) {
        try {
            new BigDecimal(str);
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    private void InitView(){
        list_Order=findViewById(R.id.list_Order);
        list_Order.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            }
        });
        btn_Query=findViewById(R.id.btn_Query);
        btn_Query.setOnClickListener(this);
        tv_ResultInfo=findViewById(R.id.tv_ResultInfo);
        edt_StyleNo =findViewById(R.id.edt_StyleNo);
        edt_StyleNo.setInputType(InputType.TYPE_NULL);
        edt_StyleNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edt_StyleNo.setInputType(InputType.TYPE_CLASS_TEXT);
                //1.得到InputMethodManager对象
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                //2.调用showSoftInput方法显示软键盘，其中view为聚焦的view组件
                imm.showSoftInput(edt_StyleNo,InputMethodManager.SHOW_FORCED);
            }
        });
        edt_StyleNo.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event!=null){
                    if ((event.getAction()==KeyEvent.ACTION_DOWN)&&(event.getKeyCode()== KeyEvent.KEYCODE_ENTER))
                    {
                        //虚拟键盘或实体键盘可考虑是否通过deviceID，或source来判断
                        // KeyEvent:KeyEvent { action=ACTION_DOWN, keyCode=KEYCODE_ENTER, scanCode=0, metaState=0, flags=0x16, repeatCount=0, eventTime=142515176, downTime=142515176, deviceId=-1, source=0x0 }
                        //隐藏键盘
                        QueryStyleNo(edt_StyleNo.getText().toString(),"Barcode");
                        return true;
                    }
                }
                return false;
            }
        });
        edt_StyleNo.requestFocus();
        edt_StyleNo.requestFocusFromTouch();

        liner_title=findViewById(R.id.liner_title);
        tv_barcodeCaption =findViewById(R.id.tv_barcodeCaption);
        txt_titleInv=findViewById(R.id.txt_titleInv);
        btn_return =findViewById(R.id.btn_return);
        btn_return.setOnClickListener(this);

    }

    // 隐藏手机键盘
    private void hideIM(View edt) {
        try {
            InputMethodManager im = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);
            IBinder windowToken = edt.getWindowToken();
            if (windowToken != null) {
                im.hideSoftInputFromWindow(windowToken, 0);
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void onClick(View view) {
        hideIM(view);
        switch (view.getId()){
            case R.id.btn_return:
                finish();//关闭当前Activity，返回上一级;
                break;
            case R.id.btn_Query:
                if ((edt_StyleNo.getText()!=null)&&(edt_StyleNo.getText().length()>0)) {
                    QueryStyleNo(edt_StyleNo.getText().toString(),"Templet");
                } else Toast.makeText(getApplicationContext(),"请先输入款号!",Toast.LENGTH_LONG).show();
                break;
            default:
                break;
        }
    }

    //登陆校验;
    private void QueryStyleNo(String paramBarcode,String paramMode){
        hideIM(edt_StyleNo);
        //组织WebService地址;
        if (FormType==null) FormType="";
        if (PrBarcode==null) PrBarcode="";
        String SqlComm="Exec App_QueryBarcode @Barcode='"+paramBarcode+"',@FormType='"+FormType+"',@QueryMode='"+(paramMode)+"',@OperType='QueryDetail',@UserNo='" + curLoginUserNo+"'";
        PrBarcode=paramBarcode;
        //调用WebService过程并处理界面更新;
        ReadSQLData(SqlComm);
    }


    //通过消息来执行处理类程序,不更新UI则不一定需要通过这个;
    final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            // TODO Auto-generated method stub
            // 根据消息ID来判断要做的事情
            int msgId = msg.what;
            hideIM(edt_StyleNo);
            edt_StyleNo.selectAll();
            edt_StyleNo.requestFocus();
            ScanSign=false;
            switch (msgId) {
                case MSG_Detail:
                    System.out.println("MSG_Detail");
                    if (detailJSON!=null) {
                        //刷新表格
                        DetailGridAdapter detailGridAdapter = new DetailGridAdapter(getApplicationContext());
                        detailGridAdapter.jsonArray = detailJSON;
                        list_Order.setAdapter(detailGridAdapter);
                    }
                    break;

                case MSG_AllowScan:
                    System.out.println("MSG_AllowScan");
                    ScanSign=false;
                    break;
                default:
                    break;
            }
        }
    };


    //直接连获取数据，与Handler配套使用;
    private void ReadSQLData(final String SQLComm) {
        ScanSign=true;
        System.out.println("SQLComm:"+SQLComm);
        Runnable run = new Runnable() {
            @Override
            public void run() {
                try{
                    JSONArray jsonArray  = DBUtil.QuerySQL2JSON(SQLComm);
                    System.out.println(jsonArray);
                    ScanSign=false;
                    //往界面转数据;
                    if ((jsonArray!=null)&&(jsonArray.length()>0)) {
                        if (jsonArray.optJSONObject(0).has("OperType")){
                            if (jsonArray.optJSONObject(0).getString("OperType").equals("QueryDetail")==true){
                                detailJSON=jsonArray;
                                //刷新界面;
                                Message msg = new Message();
                                msg.what = MSG_Detail;
                                handler.removeMessages(msg.what);
                                handler.sendMessage(msg);
                            }
                        }
                    } else{
                        Message msg = new Message();
                        msg.what = MSG_AllowScan;
                        handler.removeMessages(msg.what);
                        handler.sendMessage(msg);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    //刷新界面;
                    ScanSign=false;
                    Message msg = new Message();
                    msg.what = MSG_AllowScan;
                    handler.removeMessages(msg.what);
                    handler.sendMessage(msg);
                }

            }

        };
        new Thread(run).start();

    }



    //订单表格适配器;
    public final class DetailGridAdapter extends BaseAdapter {
        private LayoutInflater inflater;
        public DetailGridAdapter(Context context) {
            inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        private JSONArray jsonArray;

        @Override
        public int getCount() {
            return jsonArray.length();
        }

        @Override
        public Object getItem(int position) {
            try {
                return jsonArray.get(position);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) convertView = inflater.inflate(R.layout.activity_temp_item, null, false);
            String tmpValue;
            int pullVatListWidth=parent.getWidth();
            convertView.setLayoutParams(new ListView.LayoutParams((int)pullVatListWidth,(int)(parent.getHeight()/pageItemCount)));
            int backcolor=getResources().getColor(R.color.white);
            int fontcolor=getResources().getColor(R.color.text_black);
            if (position%2==1) backcolor=getResources().getColor(R.color.listContentColor);
            try {
                if (jsonArray.optJSONObject(position).has("ConColor")==true) {
                    tmpValue = jsonArray.optJSONObject(position).getString("ConColor");
                    backcolor=GetColorValue(tmpValue);
                }
                if (jsonArray.optJSONObject(position).has("ConFontColor")==true) {
                    tmpValue = jsonArray.optJSONObject(position).getString("ConFontColor");
                    fontcolor=GetColorValue(tmpValue);
                }
                TextView tv = convertView.findViewById(R.id.tv_RecTime);// 显示文字
                if (jsonArray.optJSONObject(position).has("RecTime")==true)
                    tv.setText(jsonArray.optJSONObject(position).getString("RecTime"));
                tv.setTextColor(fontcolor);
                tv.setBackgroundColor(backcolor);
                tv = convertView.findViewById(R.id.tv_StyleNo);// 显示文字
                if (jsonArray.optJSONObject(position).has("StyleNo")==true)
                    tv.setText(jsonArray.optJSONObject(position).getString("StyleNo"));
                tv.setTextColor(fontcolor);
                tv.setBackgroundColor(backcolor);
                tv = convertView.findViewById(R.id.tv_Part);// 显示文字
                if (jsonArray.optJSONObject(position).has("Part")==true)
                    tv.setText(jsonArray.optJSONObject(position).getString("Part"));
                tv.setTextColor(fontcolor);
                tv.setBackgroundColor(backcolor);
                tv = convertView.findViewById(R.id.tv_Color);// 显示文字
                if (jsonArray.optJSONObject(position).has("Color")==true)
                    tv.setText(jsonArray.optJSONObject(position).getString("Color"));
                tv.setTextColor(fontcolor);
                tv.setBackgroundColor(backcolor);
                tv = convertView.findViewById(R.id.tv_Size);// 显示文字
                if (jsonArray.optJSONObject(position).has("Size")==true)
                    tv.setText(jsonArray.optJSONObject(position).getString("Size"));
                tv.setTextColor(fontcolor);
                tv.setBackgroundColor(backcolor);
                tv = convertView.findViewById(R.id.tv_PieceNum);// 显示文字
                if (jsonArray.optJSONObject(position).has("PieceNum")==true)
                    tv.setText(jsonArray.optJSONObject(position).getString("PieceNum"));
                tv.setTextColor(fontcolor);
                tv.setBackgroundColor(backcolor);
                tv = convertView.findViewById(R.id.tv_Barcode);// 显示文字
                if (jsonArray.optJSONObject(position).has("Barcode")==true)
                    tv.setText(jsonArray.optJSONObject(position).getString("Barcode"));
                tv.setTextColor(fontcolor);
                tv.setBackgroundColor(backcolor);
            } catch (JSONException e) {
                e.printStackTrace();
                System.out.println(e.getMessage());
            }
            Log.d("msg","DetailGridAdapter_position:"+position);
            return convertView;
        }

    }



    //从字符串转颜色
    private int GetColorValue(String paramColor){
        int PenColor= Color.WHITE;
        String StrPenColor=paramColor;
        if ((StrPenColor.indexOf("#")==0)&&(StrPenColor.length()==7)){
            int testColor=Integer.parseInt(StrPenColor.substring(1),16);
            int red = (testColor & 0xff0000) >> 16;
            int green = (testColor & 0x00ff00) >> 8;
            int blue = (testColor & 0x0000ff);
            PenColor=Color.rgb(red, green, blue);
        }else {
            if (isNumer(StrPenColor)) {
                int testColor = Integer.parseInt(StrPenColor);
                int red = (testColor & 0xff0000) >> 16;
                int green = (testColor & 0x00ff00) >> 8;
                int blue = (testColor & 0x0000ff);
                PenColor = Color.rgb(red, green, blue);
            } else {
                if (StrPenColor.equals("Black") == true)
                    PenColor = Color.BLACK;
                if (StrPenColor.equals("Blue") == true)
                    PenColor = Color.BLUE;
                if (StrPenColor.equals("Yellow") == true)
                    PenColor = Color.YELLOW;
                if (StrPenColor.equals("Red") == true)
                    PenColor = Color.RED;
                if (StrPenColor.equals("Green") == true)
                    PenColor = Color.GREEN;
                if (StrPenColor.equals("Gray") == true)
                    PenColor = Color.LTGRAY;
                if (StrPenColor.equals("White") == true)
                    PenColor = Color.WHITE;
            }
        }
        return PenColor;
    }



}
